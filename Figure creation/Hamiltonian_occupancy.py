import numpy as np
import matplotlib.pyplot as plt

#Run main First

H_arr = Ham_Experiment(L,delta,s, Jglob, g, Bglob, muB, D,E,Jprime,LocB,LocJ,Jloc)
Htest0=np.kron(np.reshape(H_arr[0],(25,25)),np.eye(25))
Htest1=np.kron(np.eye(5),np.kron(np.reshape(H_arr[1],(25,25)),np.eye(5)))
Htest2=np.kron(np.eye(25),(np.reshape(H_arr[2],(25,25))))


Hodd=Htest0+Htest2
Hodd[Hodd!=0]=1

Heven=Htest1
Heven[Heven!=0]=1

Hfull=Htest0+Htest1+Htest2
Hfull[Hfull!=0]=1


plt.figure(figsize=(5,5),dpi=200)
plt.rcParams.update({'font.size': 15})
plt.imshow(np.abs(Hfull))
plt.xlabel("i",fontsize=30)
plt.ylabel("j",fontsize=30)
plt.title("$\hat{H}$",fontsize=30)
plt.show()

plt.figure(figsize=(5,5),dpi=200)
plt.rcParams.update({'font.size': 15})
plt.imshow(np.abs(Hodd))
plt.xlabel("i",fontsize=30)
plt.ylabel("j",fontsize=30)
plt.title("$\hat{H}_{odd}$",fontsize=30)
plt.show()

plt.figure(figsize=(5,5),dpi=200)
plt.rcParams.update({'font.size': 15})
plt.imshow(np.abs(Heven))
plt.xlabel("i",fontsize=30)
plt.ylabel("j",fontsize=30)
plt.title("$\hat{H}_{even}$",fontsize=30)
plt.show()