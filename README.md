# Justin BEP Matrix Product States

This Gitlab page contains the code related to the BEP of Justin Bouwmeester.
The code runs a simulation of a quantum spin-2 chain using the matrix product state formalism.

Packages required: numpy,matplotlib,string,scipy,time 

Use of the most recent version of numpy recommended. version 1.17.2+ is required due to a bugfix for the SVD function.


The folder "Figure creation" contains the code used to product the figures in the report. It is recommended not to use it as it is not user friendly.