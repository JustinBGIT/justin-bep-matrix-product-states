import numpy as np
import matplotlib.pyplot as plt
from Operators_and_Hamiltonian import Ham_Experiment,Create_Opparray,Excite_site
from MPS_Initial_State import Create_chainstate
from TimeEvolution_and_twosite import Time_evolution,Time_evolutionST2, Time_evolutionST4
import scipy.constants as sc
import time as timing
import string


"""
The main document of the code. Contains the variables and the plot function.
The index of the left-most position in the chain is 1.
"""




"""
The different values for the parameters and variables are created here
"""
#Chain Parameters
s=2                             # s=2 during the experiment
d = int(2*s+1)                  # Dimension of the spins
chi = 10                        # bond dimension. Determines where we truncate our Hilbert space
L = 5                           # length of the Chain
Start = 1                       # 0:(-2,2,-2,...) chain. 1:(2,-2,2,...) chain.
excitedloc = np.array([1])      # Index of sites that are excited. More excitations requires higher chi.
Nin = np.array([1,2,3,4,5])     #Index of sites you want to measure

#Experiment Parameters
Jglob = 0.7                                                 # coupling contant of the Nearest_Neigbour
Jprime = -0.05                                              # Artificial coupling used as replacement for the rest of the chain. Used in comparison to simulations from paper
g = 2 #2.11                                                 # g-factor
Bglob = np.array([0,0,1])                                   # Magnetic field in x,y,z direction
LocB = 0                                                    #Index of site with different local field. 0 or <L means it will not apply
LocJ = np.array([5,13])                                     #Index of site with different coupling to the right
Jloc = -0.05                                                #Strength of local coupling
muB = 5.788E-2 #10**3*sc.value("Bohr magneton in eV/T")     #Bohr magneton in meV/T
D = -1.77
E = 0.31 #0.33

#Amount of time spent evolving to the ground state before excitation. Ensures that the excitation occurs while the system is in the ground state. 
delta_ground = 1e-2         #Size of time steps used to find ground state
T_ground = 250              #Total number of time steps used to find ground state

#Time evolution after excitation.
delta_real = 1e-2               # The time step of the time_evolution
delta_im = 0#1e-3               # Damping using imaginary time evolution
T = 2500                        # The total number of timesteps of time evolution after excitation  

normalize = False           #Continuously maintain normalization. Required for imaginary time evolution.

ST = 1                 #Order of Suzuki trotter expansion used. Global error is equal to order. Order 1,2,4 available. Order 1 highly advised.
s2=1/(4-4**(1/3))

Meas=0                  #0:Measure Sz. 1: Measure Energy

delta=delta_real-1j*delta_im
delta_ground*=-1j

"""
Adjustments for the fact that arrays start at zero.
"""
LocB-=1
LocJ-=1
excitedloc-=1
Nin-=1


def Converging_Plot(g,Jglob,Jprime,L,chi,delta,T,Nin, Meas, excitedloc = [0], normalize = False, ST=1):
    """
    Creates a state with given inputs, finds the ground state, applies an excitation, and plots the resulting S_z expectation for each particle
    """
    #Creating Hamiltonian used during the simulation
    H_arr = Ham_Experiment(L,delta,s, Jglob, g, Bglob, muB, D,E,Jprime,LocB,LocJ,Jloc)
    
    #Creation of time operators
    O_arr = Create_Opparray(H_arr,L,d,delta)
    O_arrground = Create_Opparray(H_arr,L,d,delta_ground)
    
    #Creating initial state
    lambdas,gammas,loc_size = Create_chainstate(L,LocJ,chi,d,Start)
    
    #Time evolution to the ground state
    if ST==1:
        Res = Time_evolution(gammas,lambdas,T_ground,O_arrground,L,d,chi,loc_size, H_arr, Nin, Meas=1, normalize=True)
    if ST==2:
        O_arrground2 = Create_Opparray(H_arr,L,d,delta_ground/2)
        Res = Time_evolutionST2(gammas,lambdas,T_ground,O_arrground,O_arrground2,L,d,chi,loc_size, H_arr, Nin, Meas=1, normalize=True) #ST2 time evolution
    if ST==4:
        O_arrground = Create_Opparray(H_arr,L,d,delta_ground*s2/2)
        O_arrground2 = Create_Opparray(H_arr,L,d,delta_ground*s2)
        O_arrground3 = Create_Opparray(H_arr,L,d,delta_ground*(1-3*s2)/2)
        O_arrground4 = Create_Opparray(H_arr,L,d,delta_ground*(1-4*s2))
        Res = Time_evolutionST4(gammas,lambdas,T_ground,O_arrground,O_arrground2,O_arrground3,O_arrground4,L,d,chi,loc_size, H_arr, Nin, Meas=1, normalize=True) #ST4 time evolution
    
    #Excitation
    for i in excitedloc:
        gammas=Excite_site(i,s,L,lambdas,gammas, Start, LocJ)
    
    #Time Evolution after excitation
    if ST==1:
        Res = Time_evolution(gammas,lambdas,T,O_arr,L,d,chi,loc_size, H_arr, Nin, Meas, normalize)
    if ST==2:
        O_arr2 = Create_Opparray(H_arr,L,d,delta/2)
        Res = Time_evolutionST2(gammas,lambdas,T,O_arr,O_arr2,L,d,chi,loc_size, H_arr, Nin, Meas, normalize) #ST2 time evolution
    if ST == 4:
        O_arr = Create_Opparray(H_arr,L,d,delta*s2/2)
        O_arr2 = Create_Opparray(H_arr,L,d,delta*s2)
        O_arr3 = Create_Opparray(H_arr,L,d,delta*(1-3*s2)/2)
        O_arr4 = Create_Opparray(H_arr,L,d,delta*(1-4*s2))
        Res = Time_evolutionST4(gammas,lambdas,T,O_arr,O_arr2,O_arr3,O_arr4,L,d,chi,loc_size, H_arr, Nin, Meas, normalize) #ST2 time evolution
   
    time,Energy = Res

    plt.figure(figsize=(15,5))
    if Meas==0:
        for i in range(len(Nin)):
            plt.plot(Res[0][:],np.real(Res[1][:,i]),label="site "+str(Nin[i]+1))
    else:
        plt.plot(Res[0][:],np.real(Res[1][:,0]),label="Energy")
    
    plt.xlabel('Time step t')
    plt.ylabel('measured value')
    plt.legend()
    plt.show()
    return Energy
    
 
def Switching_plot(g,Jglob,Jprime,L,chi,delta,T, LocJ,Nin, Meas, normalize = False, ST=1):
    """
    Calculates 2-<S_z> for all particles up to and including max(Nin).
    """
    plotSz = []
    totalplot = np.zeros((np.max(Nin)+1,int(T/5)))
    for i in range(np.max(Nin)+1):
        Sz = Converging_Plot(g,Jglob,Jprime,L,chi,delta,int(T/5), [np.max(Nin)], Meas, [i], normalize, ST)
        meanSz = np.mean(Sz[:int(T),0])
        plotSz.append(2-meanSz)
        totalplot[i,:]=Sz[:,0]
        
    for i in range(np.max(Nin)+1):
        plt.plot(totalplot[i,:],label="site"+str(i+1))
    plt.legend()
    plt.show()
    
    Sites = list(string.ascii_uppercase[:np.max(Nin)+1])[::-1]
    plt.plot(Sites,plotSz,"o",fillstyle="none",label="Average")
    plt.legend()
    plt.show()
    
    

#"""
T1=timing.time()
if max(Nin)>L:
    print("Nin has to be smaller than or equal to L")
else:
    Energy = Converging_Plot(g,Jglob,Jprime,L,chi,delta,T, Nin, Meas, excitedloc, normalize, ST)
print("Time taken:",timing.time()-T1)
#"""
#Switching_plot(g,Jglob,Jprime,L,chi,delta,T, LocJ,Nin,Meas, normalize, ST)


